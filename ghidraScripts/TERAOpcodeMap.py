#Dumps TERA opcode info for hueristic's matching: MemoryAddress, FIDHash FH, Opcode, and Classification
#@author Kloct
#@category TERA
#@keybinding 
#@menupath 
#@toolbar
import sys
import binascii
import json
import re
import os
from ghidra.feature.fid.service import FidService
from java.lang.Long import toHexString
service = FidService()

def Log(msg):
    print("[TERAScanner]: "+msg)
#clientVersion = askString("Client Version", "Enter client version (ex:105.02GF):")
clientVersion = ""
with open(os.path.normpath(os.path.join(os.getcwd(), "tera-client-dump/ReleaseRevision.txt"))) as f:
    clientVersion = re.search("\d*\.\d*\s\w*", f.readlines()[2]).group(0).replace(" ", "")
Log("Using client version "+clientVersion)
sourceOrTarget = askChoice("Source or Target", "Choose Source or Target map.", [ "Source", "Target" ], "Source")
mapSourceFile = "foundCodes("+clientVersion+").json"
outFileName = "Map"+sourceOrTarget+"("+clientVersion+").json"
Log("Generating "+sourceOrTarget+" map.")

#a terrible method to build a lookup table for opcodes
strLookupTable = {}
if sourceOrTarget == "Source":
    mapBase = {}
    with open("OpcodeHueristicMatching/tera-data-matching/foundcodes/"+mapSourceFile) as file:
        mapBase = json.load(file)
    mapBaseKeys = mapBase.keys()
    mapBaseValues = mapBase.values()
    for i in range(len(mapBaseKeys)) :
        strLookupTable[mapBaseValues[i]] = mapBaseKeys[i]

#helper function for getting the address object
def getAddress(address, offset=0x0):
    return currentProgram.getAddressFactory().getDefaultAddressSpace().getAddress(address).add(offset)

IMAGEBASE = int(str(currentProgram.getImageBase()), 16)
def getEntryPoint(offset):
    entrypoint = offset
    foundLastUndefined = False
    while(not foundLastUndefined):
        entrypoint = entrypoint.subtract(1)
        if (getUndefinedDataAt(entrypoint)!=None or getFunctionContaining(entrypoint)!=None):
            foundLastUndefined = True
            entrypoint = entrypoint.add(1)
    return entrypoint

def getOpcodeString(code ,classification):
    try :
        return strLookupTable[int(code, 16)]
    except Exception:
        if classification=="server":
            return "SU_NEEDS_REMAP"+code
        elif classification=="client":
            return "CU_NEEDS_REMAP"+code
        else:
            print("Unclassified opode NOT named!")

foundCodes = []
builderCodes = []
CPHCodes = []
initCodes = []
handlerCodes = []
#helper function for building the map
def addCode(address, hash, opcode, mapType, classification):
    opcodeString = ""
    if sourceOrTarget == "Source":
            opcodeString = str(getOpcodeString(opcode ,classification))
    else :
        opcodeString = str(int(opcode, 16))
    fullhash = ""
    try :
        fullhash = str(toHexString(hash.getFullHash()))
    except Exception:
        fullhash = "None"
    if mapType == 0:
        builderCodes.append([str(address), fullhash, opcodeString, classification])
    elif mapType == 1:
        CPHCodes.append([str(address), fullhash, opcodeString, classification])
    elif mapType == 2:
        initCodes.append([str(address), fullhash, opcodeString, classification])
    elif mapType == 3:
        handlerCodes.append([str(address), fullhash, opcodeString, classification])

#The actual mapping functions
def HandlersArrayMap():
    monitor.setMessage("Mapping from handlers array...")
    Log("Mapping from handlers array...")
    handlersArraySignature = "\\x4f\\x8b\\xbc\\xf5"
    #4f 8b bc f5
    #ptr to array is the next 4 bytes after signature
    handlerArrayAddrBytes = getBytes(findBytes(currentProgram.getImageBase(), handlersArraySignature, 1)[0].add(0x4), 4)
    handlerArrayAddrBytes.reverse()
    handlerArrayAddr = getAddress(binascii.hexlify(handlerArrayAddrBytes), IMAGEBASE)
    for opcode in range(1, 0xffff):
        xrefs = getReferencesTo(handlerArrayAddr.add(opcode*8))
        if len(xrefs) > 0:    
            initializerAddress = xrefs[0].getFromAddress()
            initializerFunction = getFunctionContaining(initializerAddress)
            if initializerFunction != None:
                handlerAddress = getAddress(int(str(getInstructionAt(initializerAddress.add(0x25)).getInputObjects()[0]), 16))
                handlerFn = getFunctionContaining(handlerAddress)
                if handlerFn == None:
                    entrypoint = getEntryPoint(handlerAddress)
                    createFunction(entrypoint, "FUN_"+str(entrypoint))
                    handlerFn = getFunctionContaining(handlerAddress)
                
                addCode(handlerAddress, service.hashFunction(handlerFn), hex(opcode)[2:], 2, "server")
                addCode(initializerAddress, service.hashFunction(handlerFn), hex(opcode)[2:], 3, "server")

def packetBuilderMap():
    monitor.setMessage("Mapping from builder functions...")
    Log("Mapping from builder functions...")
    #signature of builder functions and opcode definer
    packetBuilderSignature = "\\x66\\x89\\x14\\x01" #Ideal refined search 66 89 14 01 4?"
    opcodeDefinedSig = "\\xba.{2}\\x00\\x00"
    #get get array of builder addresses
    builderArray = findBytes(currentProgram.getImageBase(), packetBuilderSignature, sys.maxint)
    builderArray.pop(0) #first match is always invalid
    for definerAddr in builderArray :
        definerFn = getFunctionContaining(definerAddr)
        if (definerFn==None):
            entrypoint = getEntryPoint(definerAddr)
            createFunction(entrypoint, "FUN_"+str(entrypoint))
            definerFn = getFunctionContaining(definerAddr)
        definerEntryPoint = definerFn.getEntryPoint()
        opcode = getBytes(findBytes(definerEntryPoint, opcodeDefinedSig), 3)
        opcode.pop(0) #remove instruction
        opcode.reverse()
        opcode = binascii.hexlify(bytearray(opcode))
        addCode(definerAddr, service.hashFunction(definerFn), opcode, 0, "client")

def CPHMap():
    monitor.setMessage("Mapping from client packet handlers...")
    Log("Mapping from client packet handlers...")
    # 48 89 5c 24 18 48 89 7c 24 20 41 56 48 83 ec 20 c7 41 10 00 80 00 00
    cLoginArbiterSignature = "\\x48\\x89\\x5c\\x24\\x18\\x48\\x89\\x7c\\x24\\x20\\x41\\x56\\x48\\x83\\xec\\x20\\xc7\\x41\\x10\\x00\\x80\\x00\\x00"
    cLoginArbiterAddress = findBytes(currentProgram.getImageBase(), cLoginArbiterSignature, 1)[0]
    #get address of reference to ClientPacketHeader
    CPHAddr = getAddress(int(str(getInstructionAt(cLoginArbiterAddress.add(0x17)).getInputObjects()[0]), 16))
    xrefsCPH = getReferencesTo(CPHAddr)
    for xrefCPH in xrefsCPH :
        xrefFrom = xrefCPH.getFromAddress()
        if re.match("MOV dword ptr", str(getInstructionAt(xrefFrom))) :
            func = getFunctionContaining(xrefFrom)
            if func == None :
                entrypoint = getEntryPoint(xrefFrom)
                createFunction(entrypoint, "FUN_"+str(entrypoint)) #creates func with xref as entrypoint bad but don't know how get undefined func entrypoint
                func = getFunctionContaining(xrefFrom)
            opcode = getBytes(xrefFrom.add(0x8), 2)
            opcode.reverse()
            opcode = binascii.hexlify(bytearray(opcode))
            addCode(xrefFrom, service.hashFunction(func), opcode, 1, "client")



#Separated the map functions for easier debugging
HandlersArrayMap()
CPHMap()
packetBuilderMap()

monitor.setMessage("Sorting and saving maps...")
Log("Sorting and saving maps...")
#sorting all of the individual arrays
builderCodes.sort()
CPHCodes.sort()
initCodes.sort()
handlerCodes.sort()

#combining all of the arrays into a single one
foundCodes.append(builderCodes)
foundCodes.append(CPHCodes)
foundCodes.append(initCodes)
foundCodes.append(handlerCodes)

with open(os.path.normpath(os.path.join(os.getcwd(),"ghidramaps/"+outFileName)), 'w') as json_file:
    json.dump(foundCodes, json_file, indent=2)
popup(sourceOrTarget+" map for "+clientVersion+" created! Saved to: "+outFileName)