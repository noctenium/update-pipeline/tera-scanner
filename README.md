# TERA Scanner

> A docker image containing all of the tools necessary for analyzing and generating opcode function signature mappings for TERA.

## How to Use This Project

Included in the project's repository is a docker image.

You'll need a PAT for the update pipeline in order to access dependent projects

### Project Dependencies

Your access token will need to have access to the following repositories.

| Project | Description |
| --- | --- |
|[tera-client-dump](https://gitlab.com/noctenium/update-pipeline/tera-client-dump) | contains the dumped binaries from an earlier stage for the most current TERA client version |
|[ghidramaps](https://gitlab.com/noctenium/update-pipeline/ghidramaps) | contains output map files from analysis for later stages as well as source maps for xreferencing versions |

### Running the Image

First login with docker to gain access to the registry

```Bash
docker login -u  "<your gitlab username>" --password-stdin
```

Enter your personal access token when prompted

Then to start the container enter:

```Bash
docker run registry.gitlab.com/noctenium/update-pipeline/tera-scanner -e gitlabPAT="<your gitlab personal access token>"
```
