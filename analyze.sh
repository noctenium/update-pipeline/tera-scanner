#!/bin/bash
#Download most recent dumpfile
cd /app
git clone https://oauth2:$gitlabPAT@gitlab.com/noctenium/update-pipeline/tera-client-dump.git/

#Download most recent ghidraMaps
git clone https://oauth2:$gitlabPAT@gitlab.com/noctenium/update-pipeline/ghidramaps.git/

#Create Ghidra Project and run script
ghidra/ghidra*/support/analyzeHeadless \
    projects TERAScanner \
    -import tera-client-dump/TERA_dump.exe \
    -ScriptPath "$(pwd)/ghidraScripts" \
    -propertiesPath "$(pwd)/ghidraScripts" \
    -postScript "TERAOpcodeMap.py" \

#Commit updated mapfile to ghidraMaps
clientVersion=$(grep -E '\.' tera-client-dump/ReleaseRevision.txt | awk '{print $2,$3}' | head -1)
cd ghidramaps
git config --global user.name "TERA Scanner CI"
git config --global user.email "Upfrontsuperior@gmail.com"
git add .
git commit -m "Added map for $clientVersion"
git push
cd ..