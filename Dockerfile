FROM ubuntu:latest
#install dependencies default=jre, ghidra
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y update && apt-get -y install jq wget curl git default-jre-headless default-jdk unzip

#download and unzip ghidra
RUN wget "$(curl -s https://api.github.com/repos/NationalSecurityAgency/ghidra/releases/latest | jq -r ".assets[] | .browser_download_url")" && mkdir /app && unzip ghidra*.zip -d /app/ghidra && rm -rf ghidra*.zip

ENV gitlabPAT = ""
COPY analyze.sh /app/analyze.sh
COPY ghidraScripts /app/ghidraScripts
COPY projects /app/projects